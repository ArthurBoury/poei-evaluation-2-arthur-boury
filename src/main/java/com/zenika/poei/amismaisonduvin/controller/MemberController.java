package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.exception.CreationException;
import com.zenika.poei.amismaisonduvin.domain.exception.NotFoundException;
import com.zenika.poei.amismaisonduvin.repository.Repository;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MemberController {

    private final Repository<Member> memberRepository;

    public MemberController(Repository<Member> memberRepository) {
        this.memberRepository = memberRepository;
    }

    @GetMapping("/members")
    public List<Member> getAllMembers() {
        return memberRepository.findAll();
    }

    @GetMapping("/members/{id}")
    public Member getMemberById(@PathVariable("id") int id) {
        try {
            return memberRepository.findById(id);
        } catch (DataAccessException e) {
            throw new CreationException("Le membre est introuvable !");
        }
    }

    @PostMapping("/members")
    public Member addMember(@RequestBody Member member) {
        try {
            return memberRepository.save(member);
        } catch (DataAccessException e) {
            throw new CreationException("Erreur lors de l'ajout d'un nouveau membre...");
        }
    }

    @PutMapping("/members/{id}")
    public Integer updateMember(@PathVariable("id") int id, @RequestBody Member member) {
        Member memberToUpdate = memberRepository.findById(id);
        memberToUpdate.modify(member);
        return memberRepository.save(memberToUpdate).getId();
    }

    @DeleteMapping("/members/{id}")
    public Member deleteMember(@PathVariable int id) {
        Member member = memberRepository.findById(id);
        return memberRepository.delete(member);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<HttpError> errorCreationMember(NotFoundException e) {
        HttpError httpError = new HttpError(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpError);
    }

    @ExceptionHandler(CreationException.class)
    public ResponseEntity<HttpError> errorCreationMember(CreationException e) {
        HttpError httpError = new HttpError(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpError);
    }
}
