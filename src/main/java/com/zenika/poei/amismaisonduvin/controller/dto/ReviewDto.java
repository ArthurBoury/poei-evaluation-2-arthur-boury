package com.zenika.poei.amismaisonduvin.controller.dto;

public class ReviewDto {

    private Integer memberId;
    private int score;
    private String comment;

    public ReviewDto(Integer memberId, int score, String comment) {
        this.memberId = memberId;
        this.score = score;
        this.comment = comment;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public int getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }
}
