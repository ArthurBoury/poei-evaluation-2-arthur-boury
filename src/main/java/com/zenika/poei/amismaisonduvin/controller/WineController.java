package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.controller.dto.ReviewDto;
import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.domain.exception.CreationException;
import com.zenika.poei.amismaisonduvin.domain.exception.NotFoundException;
import com.zenika.poei.amismaisonduvin.repository.JdbcMemberRepository;
import com.zenika.poei.amismaisonduvin.repository.JdbcWineRepository;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
public class WineController {
    private final JdbcWineRepository wineRepository;
    private final JdbcMemberRepository memberRepository;

    public WineController(JdbcWineRepository wineRepository, JdbcMemberRepository memberRepository) {
        this.wineRepository = wineRepository;
        this.memberRepository = memberRepository;
    }

    @GetMapping("/wines")
    public List<Wine> getAllBottles(@RequestParam(value = "region", required = false) String region) {
        if (!Objects.equals(region, null)) {
            return wineRepository.findByRegion(region);
        }
        return wineRepository.findAll();
    }

    @GetMapping("/wines/{id}")
    public Wine getBottle(@PathVariable("id") int id) {
        try {
            return wineRepository.findById(id);
        } catch (DataAccessException e) {
            throw new NotFoundException("La référence est introuvable....");
        }
    }

    @PostMapping("/wines")
    public Wine addBottles(@RequestBody Wine wine) {
        try {
            return wineRepository.save(wine);
        } catch (DataAccessException e) {
            throw new CreationException("Erreur lors de la création de la référence...");
        }
    }

    @PutMapping("/wines/{id}")
    public Wine updateBottles(@PathVariable("id") int id, @RequestBody Wine wine) {
        Wine bottle = wineRepository.findById(id);
        bottle.modify(wine);
        return wineRepository.save(bottle);
    }

    @DeleteMapping("wines/{id}")
    public Wine deleteBottles(@PathVariable("id") int id) {
        Wine wineBottle = wineRepository.findById(id);
        return wineRepository.delete(wineBottle);
    }

    @PostMapping("/wines/{id}/reviews")
    public String addReview(@PathVariable int id, @RequestBody ReviewDto reviewDto) {
        Review review = new Review(
                memberRepository.findById(reviewDto.getMemberId())
                , reviewDto.getScore()
                , reviewDto.getComment());
        Wine bottle = wineRepository.findById(id);
        bottle.addReview(review);
        wineRepository.save(bottle);
        return "Votre review à bien été ajoutée !";
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<HttpError> errorWineNotFound(NotFoundException e) {
        HttpError httpError = new HttpError(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(httpError);
    }

    @ExceptionHandler(CreationException.class)
    public ResponseEntity<HttpError> errorWineCreation(CreationException e) {
        HttpError httpError = new HttpError(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpError);
    }

}
