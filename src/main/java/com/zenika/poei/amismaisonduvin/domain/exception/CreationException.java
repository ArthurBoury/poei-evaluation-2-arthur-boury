package com.zenika.poei.amismaisonduvin.domain.exception;

import org.springframework.dao.DataAccessException;

public class CreationException extends DataAccessException {


    public CreationException(String msg) {
        super(msg);
    }
}
