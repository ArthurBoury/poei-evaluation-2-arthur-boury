package com.zenika.poei.amismaisonduvin.domain;

import com.zenika.poei.amismaisonduvin.domain.exception.InvalidMemberException;
import org.springframework.util.StringUtils;

public class Member {

    private Integer id;
    private String pseudonym;
    private String firstName;
    private String lastName;

    public Integer getId() {
        return id;
    }

    public Member setId(Integer id) {
        this.id = id;

        return this;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public Member setPseudonym(String pseudonym) {
        if (!StringUtils.hasText(pseudonym)) {
            throw new InvalidMemberException("Pseudonym is empty");
        }

        this.pseudonym = pseudonym;

        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Member setFirstName(String firstName) {
        if (!StringUtils.hasText(firstName)) {
            throw new InvalidMemberException("FirstName is empty");
        }

        this.firstName = firstName;

        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Member setLastName(String lastName) {
        if (!StringUtils.hasText(lastName)) {
            throw new InvalidMemberException("LastName is empty");
        }

        this.lastName = lastName;

        return this;
    }

    public void modify(Member member) {
        if (member.getPseudonym() != null) {
            this.pseudonym = member.getPseudonym();
        }
        if (member.getFirstName() != null) {
            this.firstName = member.getFirstName();
        }
        if (member.getLastName() != null) {
            this.lastName = member.getLastName();
        }
    }
}
