package com.zenika.poei.amismaisonduvin.domain.exception;

import org.springframework.dao.DataAccessException;

public class NotFoundException extends DataAccessException {
    public NotFoundException(String msg) {
        super(msg);
    }
}
