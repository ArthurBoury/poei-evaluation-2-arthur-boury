package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class JdbcWineRepository implements Repository<Wine> {
    private static RowMapper<Wine> WINE_ROW_MAPPER = (rs, rowNum) -> new Wine()
            .setId(rs.getInt("id"))
            .setAppellation(rs.getString("appellation"))
            .setRegion(rs.getString("region"))
            .setVintage(Year.of(rs.getInt("vintage")))
            .setColour(rs.getString("colour"))
            .setVariety(rs.getString("variety"));

    private static RowMapper<Review> REVIEW_ROW_MAPPER = (rs, rowNum) ->
            new Review(new Member()
                    .setId(rs.getInt("id"))
                    .setPseudonym(rs.getString("pseudonym"))
                    .setFirstName(rs.getString("firstname"))
                    .setLastName(rs.getString("lastname")),
                    rs.getInt("score"), rs.getString("comment"));

    private final JdbcTemplate jdbcTemplate;

    public JdbcWineRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Wine> findAll() {
        List<Wine> bottles = jdbcTemplate.query("select * from wine order by id", WINE_ROW_MAPPER);
        for (Wine bottle : bottles) {
            List<Review> reviews = jdbcTemplate.query("select m.id, pseudonym, firstname, lastname, score, comment from wine left join review r on wine.id = r.wine_id left join member m on r.member_id = m.id and r.wine_id = wine.id where wine_id = ?"
                    , REVIEW_ROW_MAPPER
                    , bottle.getId());
            reviews.forEach(bottle::addReview);
        }
        return bottles;
    }

    @Override
    public Wine findById(int id) {
        Wine bottle = jdbcTemplate.queryForObject("select * from wine where id = ?", WINE_ROW_MAPPER, id);
        List<Review> reviews = jdbcTemplate.query("select m.id, pseudonym, firstname, lastname, score, comment from wine left join review r on wine.id = r.wine_id left join member m on r.member_id = m.id and r.wine_id = wine.id where wine_id = ?"
                , REVIEW_ROW_MAPPER
                , id);
        reviews.forEach(bottle::addReview);
        return bottle;
    }

    public List<Wine> findByRegion(String region) {
        List<Wine> bottles = jdbcTemplate.query("select * from wine where region = ?", WINE_ROW_MAPPER, region);
        for (Wine bottle : bottles) {
            List<Review> reviews = jdbcTemplate.query("select m.id, pseudonym, firstname, lastname, score, comment from wine left join review r on wine.id = r.wine_id left join member m on r.member_id = m.id and r.wine_id = wine.id where wine_id = ?"
                    , REVIEW_ROW_MAPPER
                    , bottle.getId());
            reviews.forEach(bottle::addReview);
        }
        return bottles;
    }

    @Override
    public Wine save(Wine wine) {
        if (Objects.equals(wine.getId(), null)) {
            wine = jdbcTemplate.queryForObject("insert into wine (appellation, region, vintage, colour, variety) values (?,?,?,?,?) returning *"
                    , WINE_ROW_MAPPER
                    , wine.getAppellation()
                    , wine.getRegion()
                    , wine.getVintage().getValue()
                    , wine.getColour()
                    , wine.getVariety());
        } else {
            jdbcTemplate.update("delete from review where wine_id = ?", wine.getId());
            for (Review review : wine.getReviews()) {
                jdbcTemplate.update("insert into review (wine_id, member_id, score, comment) values (?,?,?,?)", wine.getId(), review.getMember().getId(), review.getScore(), review.getComment());
            }
            wine = jdbcTemplate.queryForObject("update wine set appellation = ?, region = ?, vintage = ?, colour = ?, variety = ? where id = ? returning *"
                    , WINE_ROW_MAPPER
                    , wine.getAppellation()
                    , wine.getRegion()
                    , wine.getVintage().getValue()
                    , wine.getColour()
                    , wine.getVariety()
                    , wine.getId());
        }
        return wine;
    }

    @Override
    public Wine delete(Wine wine) {
        jdbcTemplate.update("delete from review where wine_id = ?", wine.getId());
        jdbcTemplate.update("delete from wine where id = ?", wine.getId());
        return wine;
    }
}
