package com.zenika.poei.amismaisonduvin.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T> {
    List<T> findAll();

    T findById(int id);

    T save(T element);

    T delete(T element);
}
