package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Review;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Component
public class JdbcMemberRepository implements Repository<Member> {
    private static RowMapper<Member> MEMBER_ROW_MAPPER = (rs, rowNum) -> new Member()
            .setId(rs.getInt("id"))
            .setPseudonym(rs.getString("pseudonym"))
            .setFirstName(rs.getString("firstname"))
            .setLastName(rs.getString("lastname"));

    private final JdbcTemplate jdbcTemplate;

    public JdbcMemberRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Member> findAll() {
        return jdbcTemplate.query("select * from member order by id", MEMBER_ROW_MAPPER);
    }

    @Override
    public Member findById(int id) {
        return jdbcTemplate.queryForObject("select * from member where id = ?", MEMBER_ROW_MAPPER, id);
    }

    @Override
    public Member save(Member member) {
        if (Objects.equals(member.getId(), null)) {
            member = jdbcTemplate.queryForObject("insert into member (pseudonym, firstname, lastname) values (?, ?, ?) returning *", MEMBER_ROW_MAPPER, member.getPseudonym(), member.getFirstName(), member.getLastName());
        } else {
            member = jdbcTemplate.queryForObject("update member set pseudonym = ?, firstname = ?, lastname = ? where id = ? returning *", MEMBER_ROW_MAPPER, member.getPseudonym(), member.getFirstName(), member.getLastName(), member.getId());
        }
        return member;
    }

    @Override
    public Member delete(Member member) {
        return jdbcTemplate.queryForObject("delete from member where id = ? returning *", MEMBER_ROW_MAPPER, member.getId());
    }
}
