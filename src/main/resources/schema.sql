create table if not exists wine
(
    id          serial
        constraint wine_pk
            primary key,
    appellation text    not null,
    region      text    not null,
    vintage     integer not null,
    colour      text    not null,
    variety     text    not null
);

alter table wine owner to postgres;

create unique index wine_id_uindex
    on wine (id);

create table if not exists member
(
    id        serial
        constraint member_pk
            primary key,
    pseudonym text not null,
    firstname text not null,
    lastname  text not null
);

alter table member owner to postgres;

create unique index member_id_uindex
    on member (id);

create table if not exists review
(
    wine_id   integer not null
        constraint review_wine_id_fk
            references wine,
    member_id integer not null
        constraint review_member_id_fk
            references member,
    score     integer,
    comment   text,
    constraint review_pk
        primary key (wine_id, member_id)
);

alter table review owner to postgres;



