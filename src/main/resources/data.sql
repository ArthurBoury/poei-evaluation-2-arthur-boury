insert into member (pseudonym, firstname, lastname)
values ('Spiderman', 'Peter', 'Parker');

insert into member (pseudonym, firstname, lastname)
values ('Batman', 'Bruce', 'Wayne');

insert into member (pseudonym, firstname, lastname)
values ('Superman', 'Clark', 'Kent');

insert into member (pseudonym, firstname, lastname)
values ('Dardevil', 'Matthiew', 'Murdock');

insert into wine (appellation, region, vintage, colour, variety)
values ('Bonnezeaux', 'Loire', 1996, 'blanc', 'Chenin blanc');

insert into wine (appellation, region, vintage, colour, variety)
values ('Saumur', 'Loire', 2009, 'rouge', 'Cabernet Franc');

insert into wine (appellation, region, vintage, colour, variety)
values ('Muscadet Coteaux de la Loire', 'Loire', 2017, 'blanc', 'Melon de Bourgogne');

insert into wine (appellation, region, vintage, colour, variety)
values ('Grignan-les-Adhémar', 'Rhone', 2016, 'rouge', 'Syrah');

insert into wine (appellation, region, vintage, colour, variety)
values ('Comtés Rhodaniens', 'Rhone', 2019, 'blanc', 'Viognier');

insert into wine (appellation, region, vintage, colour, variety)
values ('Patrimonio', 'Provence-corse', 2018, 'rosé', 'Nielluciu');

insert into review (wine_id, member_id, score, comment)
values (6, 1, 5, 'Un rosé de prestige, idéal pour accompagné un bon repas');

insert into review (wine_id, member_id, score, comment)
values (3, 2, 3, 'Une promesse de prestige qui reste assez moyen finalement');

insert into review (wine_id, member_id, score, comment)
values (4, 3, 4, 'Une valeur sûre dans le Rhône...');

insert into review (wine_id, member_id, score, comment)
values (1, 4, 5, 'La crème de la crème des moelleux, à déguster les yeux fermé!');

insert into review (wine_id, member_id, score, comment)
values (1, 2, 2, 'Une horreur en bouche, trop acide et vraiment pas recommandable');

